//P1421 小玉买文具 https://www.luogu.com.cn/problem/P1421

#include <iostream>
#include <cmath>

using namespace std;

int main() {

	int yuan, jiao; 
	cin >> yuan >> jiao;

	double money = yuan + jiao / 10.0; // 把元和角合并成浮点数
	cout << floor(money / 1.9); // 向下取整

	return 0;
}