// https://www.luogu.com.cn/problem/P1035

#include <iostream>
using namespace std;

int main()
{
    int k = 0;
    cin >> k;

    int n = 0;
    double sum = 0;

    while (sum <= k) {
        n += 1;
        sum += (1.0 / n);
    }
    cout << n;
	return 0;
}
