#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int n; //用电的度数
    cin >> n;

    double sum = 0; // 价格，因为有小数点，用double浮点类型

    if (n <= 150) // 小于等于150度，直接计算
    {
        sum = n * 0.4463;
    }
    else if (n >= 151 and n <= 400) // 大于150度，小于等于400度
    {
        sum = 150 * 0.4463;        // 150度的钱
        sum += (n - 150) * 0.4663; // 150-400之间的读书
    }
    else if (n >= 401) // 大于400度
    {
        sum = 150 * 0.4463;
        sum += (400 - 150) * 0.4663;
        sum += (n - 400) * 0.5663; // 超出400的费用
    }

    printf("%.1f", sum); // 保留一位小数

    return 0;
}