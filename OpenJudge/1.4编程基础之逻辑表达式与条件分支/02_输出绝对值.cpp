// http://noi.openjudge.cn/ch0104/02/

#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;

int main(void)
{
    float n;
    cin >> n;
    cout << fixed << setprecision(2) << abs(n);
    return 0;
}