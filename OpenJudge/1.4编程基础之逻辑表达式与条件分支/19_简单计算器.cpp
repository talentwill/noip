// OpenJudge - 19:简单计算器: http://noi.openjudge.cn/ch0104/19/

#include <iostream>
using namespace std;

int main()
{
    int a, b;
    char op;
    cin >> a >> b >> op;

    switch (op)
    {
    case '+':
        cout << a + b;
        break;
    case '-':
        cout << a - b;
        break;
    case '*':
        cout << a * b;
        break;
    case '/':
        if (b == 0)
            cout << "Divided by zero!";
        else
            cout << a / b;
        break;
    default:
        cout << "Invalid operator!";
    }

    return 0;
}