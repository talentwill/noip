// OpenJudge - 10:有一门课不及格的学生: http://noi.openjudge.cn/ch0104/10/

#include <iostream>
using namespace std;

int main()
{
    int scoreA, scoreB; // 语文，数学成绩
    cin >> scoreA >> scoreB;

    if (scoreA < 60 and scoreB >= 60)
    {
        cout << 1;
    } 
    else if (scoreB < 60 and scoreA >= 60)
    {
        cout << 1;
    }
    else
    {
        cout << 0;
    }

    return 0;
}