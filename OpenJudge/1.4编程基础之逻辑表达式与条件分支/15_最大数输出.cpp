// OpenJudge - 15:最大数输出: http://noi.openjudge.cn/ch0104/15

#include <iostream>
using namespace std;

// 打擂法
int main()
{
    int a, b, c;
    cin >> a >> b >> c;
    
    int maxN = a; // 先暂定a为最大数
    if (b > maxN)
    {
        maxN = b; // b大，替换成b
    }
    if (c > maxN)
    {
        maxN = c; // c大，替换成c
    }

    cout << maxN;

    return 0;
}