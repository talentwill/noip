// OpenJudge - 13:分段函数: http://noi.openjudge.cn/ch0104/13/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    float x, y;
    cin >> x;

    if (x >= 0 and x < 5)
    {
        y = -x + 2.5;
    }
    else if (x >= 5 and x < 10)
    {
        y = 2 - 1.5 * (x - 3) * (x - 3);
    }
    else if (x >= 10 and x < 20)
    {
        y = x / 2 - 1.5;
    }

    printf("%.3f", y);

    return 0;
}