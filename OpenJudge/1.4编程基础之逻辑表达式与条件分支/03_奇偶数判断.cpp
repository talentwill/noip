// http://noi.openjudge.cn/ch0104/03/

#include <iostream>
using namespace std;

int main(void)
{
    int n;
    cin >> n;
    cout << (n % 2 == 0 ? "even" : "odd");
    return 0;
}
