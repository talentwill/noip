#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int n; // 总的苹果数量
    int x; // 每x小时吃掉一个苹果
    int y; // 经过y小时
    cin >> n >> x >> y;

    int m = ceil((1.0 / x) * y); // 算出y小时吃掉的苹果数，ceil向上取整

    if (n - m < 0) // 吃掉的苹果大于库存
        cout << 0;
    else
        cout << n - m; // 总的-吃掉的

    return 0;
}