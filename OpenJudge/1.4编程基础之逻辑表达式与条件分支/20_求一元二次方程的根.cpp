// 20:求一元二次方程的根: http://noi.openjudge.cn/ch0104/20

#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

// 这道题公式属于初中的求根公式，内容不理解没关系，只用把公式转成代码即可。
// 但是要注意细节，否则容易出错。
int main()
{
    double a, b, c;
    cin >> a >> b >> c;

    if (b * b == 4 * a * c)
    {
        cout << fixed << setprecision(5) << "x1=x2=" << -b / (2 * a) << endl;
    }
    else if (b * b > 4 * a * c)
    {
        cout << fixed << setprecision(5) << "x1=" << (-b + sqrt(b * b - 4 * a * c)) / (2 * a);
        cout << ";x2=" << (-b - sqrt(b * b - 4 * a * c)) / (2 * a);
    }
    else if (b * b < 4 * a * c)
    {
        double real = b == 0 ? 0: -b / (2 * a); // 注意：如果b为0，输出0，避免输出-0的情况
        double imaginary = sqrt(4 * a * c - b * b) / (2 * a);

        cout << fixed << setprecision(5) << "x1=" << real << "+" << imaginary;
        cout << "i;x2=" << real << "-" << imaginary << "i";
    }

    return 0;
}