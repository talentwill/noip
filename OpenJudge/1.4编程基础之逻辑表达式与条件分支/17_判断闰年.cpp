// OpenJudge - 17:判断闰年: http://noi.openjudge.cn/ch0104/17

#include <iostream>
using namespace std;

// 这题根据题目的提示，写出对应的条件即可。

int main()
{
    int n;
    cin >> n;

    if (n % 4 == 0) // 能被4整除的大多是闰年
    {
        if (n % 100 == 0 and n % 400 != 0) // 排除：但能被100整除而不能被400整除的年份不是闰年
        {
            cout << "N";
        }
        else if (n % 3200 == 0) // 排除：能被3200整除的也不是闰年
        {
            cout << "N";
        }
        else // 排除2个例外，剩下的能被4整除的
        {
            cout << "Y";
        }
    }
    else // 不能被4整除的年份
    {
        cout << "N";
    }

    return 0;
}