#include <iostream>
using namespace std;

int main() {
    int n;
    cin >> n;

    bool d3 = n % 3 == 0; // 能否被3整除
    bool d5 = n % 5 == 0; // 能否被5整除
    bool d7 = n % 7 == 0; // 能否被7整除

    if (d3) // 能被3整除
    {
        cout << "3";
        if (d5)
        {
            cout << " 5";
        }
        if (d7)
        {
            cout << " 7";
        }
    }
    else if (d5) // 不能被3整除，能被5整除
    {
        cout << "5";
        if (d7)
        {
            cout << " 7";
        }
    }
    else if (d7) // 不能被3、5整除，能被7整除
    {
        cout << "7";
    }
    else
    {
        cout << "n";
    }

    return 0;
}