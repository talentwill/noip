#include <iostream>
using namespace std;

int main()
{
    int n;
    cin >> n;

    double bike = n / 3.0 + 27 + 23;
    double walk = n / 1.2;

    if (bike < walk)
    {
        cout << "Bike";
    }
    else if (bike > walk)
    {
        cout << "Walk";
    }
    else
    {
        cout << "All";
    }

    return 0;
}