#include <iostream>
using namespace std;

int main()
{
    int x; // x表示星期几
    cin >> x;

    if (x >= 10 && x <= 99)
        cout << 1;
    else
        cout << 0;

    return 0;
}