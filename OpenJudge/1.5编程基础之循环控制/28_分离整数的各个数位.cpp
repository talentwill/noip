// http://noi.openjudge.cn/ch0105/28/

#include <iostream>
using namespace std;

int main(void)
{
    int n = 0;
    cin >> n;
    
    while (n > 0)
    {
        cout << n % 10 << " ";
        n /= 10;
    }
    return 0;
}
