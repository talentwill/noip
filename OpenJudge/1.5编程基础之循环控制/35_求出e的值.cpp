#include <iostream>
using namespace std;

int main()
{
    int n = 0;
    cin >> n;

    double p = 1;
    double sum = 1;
    for (int i = 1; i <= n; i++)
    {
        p *= i;
        sum += 1 / p;
    }

    printf("%.10f", sum);

    return 0;
}