#include <iostream>
using namespace std;

int main()
{
    int N, m;
    cin >> N >> m;

    int count = 0;
    for (int i = 0; i < N; i++)
    {
        int num = 0;
        cin >> num;
        if (num == m)
        {
            count++;
        }
    }

    cout << count;

    return 0;
}