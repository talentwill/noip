#include <iostream>
using namespace std;

int main()
{
    float x = 0;
    int n = 0;
    cin >> x >> n;

    float p = 1;
    float sum = 1;
    for (int i = 1; i <= n; i++)
    {
        p *= x;
        sum += p;
    }

    printf("%.2f", sum);

    return 0;
}