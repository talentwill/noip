#include <iostream>
using namespace std;

int main()
{
    int N;
    cin >> N;

    // 单次
    int gold = 0;
    int silver = 0;
    int bronze = 0;

    // 总数
    int total_gold = 0;
    int total_silver = 0;
    int total_bronze = 0;

    for (int i = 0; i < N; i++)
    {
        cin >> gold >> silver >> bronze;
        total_gold += gold;
        total_silver += silver;
        total_bronze += bronze;
    }

    cout << total_gold << " " << total_silver << " " << total_bronze << " " << (total_gold + total_silver + total_bronze);

    return 0;
}