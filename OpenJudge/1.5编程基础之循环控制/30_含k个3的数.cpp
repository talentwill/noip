// http://noi.openjudge.cn/ch0105/30

#include <iostream>
using namespace std;

int main()
{
	int m, k;
	cin >> m >> k;
	
	if (m % 19 == 0)
	{
		while (m != 0)
		{
			if (m % 10 == 3) {
				k--; // 直接用K来计数，如果存在一个3，则K计数-1
			}
			m /= 10;
		}
	}
	// 如果K=0，表示数字存在K个3。
	cout << (k == 0 ? "YES\n" : "NO\n");
	return 0;
}
