// http://noi.openjudge.cn/ch0105/29/

#include <iostream>
using namespace std;

int main(void)
{
    int n = 0, m = 0;
    cin >> n;

    while (n != 0)
    {
    	m *= 10;
        m += n % 10; // 取模，得到个位的数字
        n /= 10; // 十进制，移除个位的数字
    }
    cout << m;
    return 0;
}
