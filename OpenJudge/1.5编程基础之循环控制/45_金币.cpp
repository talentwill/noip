// http://noi.openjudge.cn/ch0105/45

#include <iostream>
using namespace std;

int main()
{
    int days = 0;
    cin >> days; // 发工资的天数

    int coins = 0; // 获得的金币数量
    int N = 1; // 每N天发N个金币，不断累加

    while (days > 0)
    {
        if (days - N >= 0) { // 剩余天数 > N天
            coins += N * N;
        }
        else {
            coins += days * N;
        }

        days -= N; // 扣除第N轮的天数，N天
        N++;
    }
    cout << coins;
    return 0;
}
