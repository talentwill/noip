#include <iostream>
using namespace std;

int main()
{
    int N; // N = 学生的人数
    cin >> N;

    int age = 0;
    float sum = 0;

    for (int i = 0; i < N; i++)
    {
        cin >> age;
        sum += age; // 求和
    }

    float average = sum / N; // 计算平均年龄
    printf("%.2f", average);

    return 0;
}