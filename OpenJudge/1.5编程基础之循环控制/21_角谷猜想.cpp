// http://noi.openjudge.cn/ch0105/21/

#include <iostream>
using namespace std;

int main(void)
{
    long n = 0; // 因为int类型的数字 * 3 之后，会超出int的范围，所以，采用long，防止数字过大溢出
    cin >> n;

    while (n != 1)
    {
        cout << n;
        if (n % 2 == 1) { // 奇数
            n = n * 3 + 1;
            cout << "*3+1=";
        }
        else { // 偶数
            n /= 2;
            cout << "/2=";
        }
        cout << n << endl;
    }
    cout << "End\n";
    return 0;
}