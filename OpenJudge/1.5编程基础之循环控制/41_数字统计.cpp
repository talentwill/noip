#include <iostream>
using namespace std;

int main()
{
    int L, R;
    cin >> L >> R;

    int count = 0;
    for (int i = L; i <= R; i++)
    {
        int temp = i;
        do {
            int r = temp % 10;
            if (r == 2)
            {
                count++;
            }
            temp /= 10;
        } while (temp > 0);
    }
    cout << count;
    return 0;
}