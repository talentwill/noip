// 19:A*B问题
// http://noi.openjudge.cn/ch0103/19/

#include <iostream>
using namespace std;

int main()
{
    long a, b;  // 这里要用long，因为a乘以b的结果，
                // 乘积可能会非常大，超出int可以容纳的范围。
                // long 是64位二进制，int是32位
    std::cin >> a >> b;
    std::cout << a * b;
    return 0;
}