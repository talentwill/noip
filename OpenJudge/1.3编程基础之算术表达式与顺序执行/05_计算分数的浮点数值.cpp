// http://noi.openjudge.cn/ch0103/05/

#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;

int main(void)
{
    double a = 0, b = 0;
    cin >> a >> b;
    cout << fixed << setprecision(9) << a / b;
    return 0;
}