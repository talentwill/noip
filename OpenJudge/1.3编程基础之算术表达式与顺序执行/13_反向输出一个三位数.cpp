// 13:反向输出一个三位数
// http://noi.openjudge.cn/ch0103/13/

#include <iostream>
using namespace std;

int main()
{
    int num;
    cin >> num;
    
    int a = num / 100;  //百万
    int b = (num % 100) / 10; // 十位
    int c = num % 10;   // 个位

    cout << c << b << a << endl;

    return 0;
}