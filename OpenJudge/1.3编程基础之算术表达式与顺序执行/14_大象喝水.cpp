// 14:大象喝水
// http://noi.openjudge.cn/ch0103/14/

#include <iostream>
#include <cmath>

using namespace std;

int main() {
    
    int h; // 高
    int r; // 半径
    cin >> h >> r;
    
    // 1. 计算出每个桶可以装多少升水，转换成升为单位
    double size = 3.1415926 * h * r * r / 1000; 
    
    // 2. 大象要喝20升，除以每个桶的大小，得出桶的数量
    double bucket = 20 / size; 
    
    cout << ceil(bucket); // 3. 向上取整，比如2.12，向上取整为3
    
    return 0;
}
