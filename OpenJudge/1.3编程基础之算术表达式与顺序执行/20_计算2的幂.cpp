// 20:计算2的幂 http://noi.openjudge.cn/ch0103/20/

#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int n;
    cin >> n;
    double num = pow(2, n); // 注意pow返回的类型是double
    cout << (int)num; // 答案要求是整数，所以需要强制转换成int型
    return 0;
}