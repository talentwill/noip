// 06:甲流疫情死亡率
// http://noi.openjudge.cn/ch0103/06/

#include <iostream>
using namespace std;

int main()
{
    double confirmed, dead;
    cin >> confirmed >> dead;
    printf("%.3f%%\n", (dead / confirmed) * 100);
    return 0;
}