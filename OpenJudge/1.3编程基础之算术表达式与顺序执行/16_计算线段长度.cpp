// 16:计算线段长度: http://noi.openjudge.cn/ch0103/16/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    double Xa, Ya, Xb, Yb;
    cin >> Xa >> Ya >> Xb >> Yb;
    
    // 利用公式求线段长度
    double len = sqrt((((Xa-Xb) * (Xa-Xb))) + ((Ya-Yb) * (Ya-Yb)));
    printf("%0.3f", len); //保留3位小数
    
    return 0;
}
