// 02:计算(a+b)*c的值
// http://noi.openjudge.cn/ch0103/02/

#include <iostream>
using namespace std;

int main()
{
    int a, b, c;
    std::cin >> a >> b >> c;
    std::cout << (a + b) * c;
    return 0;
}