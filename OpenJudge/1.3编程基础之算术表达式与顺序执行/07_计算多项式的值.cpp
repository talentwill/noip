// 07:计算多项式的值
// http://noi.openjudge.cn/ch0103/07/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    double x, a, b, c, d;
    cin >> x >> a >> b >> c >> d;
    double result = a*x*x*x + b*x*x + c*x + d;
    printf("%.7f", result);
    return 0;
}
