// 18:等差数列末项计算: http://noi.openjudge.cn/ch0103/18/

#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int a1, a2, n;
    cin >> a1 >> a2 >> n;
    cout << a1 + (n-1) * (a2-a1);
    return 0;
}
