// 17:计算三角形面积 http://noi.openjudge.cn/ch0103/17/

#include <iostream>
#include <cmath>
#include <iomanip> // 精确到小数点后几位的头文件

using namespace std;

int main() {
    
    double x1, y1, x2, y2, x3, y3;
    cin >> x1 >> y1 >> x2 >> y2 >> x3 >> y3;
    
    // 两点距离公式，计算 a，b，c 三条边长
    double a = sqrt(pow((x1-x2),2) + pow((y1-y2),2));
    double b = sqrt(pow((x1-x3),2) + pow((y1-y3),2));
    double c = sqrt(pow((x2-x3),2) + pow((y2-y3),2));
    
    // 海伦公式
    double p = (a + b + c) / 2;
    double s = sqrt(p * (p - a) * (p - b) * (p - c));

    // 保留2位小数
    cout <<  fixed << setprecision(2) << s << endl; 

    return 0;
}