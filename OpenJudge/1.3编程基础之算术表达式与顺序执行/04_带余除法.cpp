// 04:带余除法
// http://noi.openjudge.cn/ch0103/04/

#include <iostream>

using namespace std;

int main() {
    
    int dividend = 0; // 除数
    int divisor = 0;  // 被除数
    
    cin >> dividend >> divisor;
    
    int quotient = dividend / divisor;  // 求商，用 “/”
    int remainder = dividend % divisor; // 求余数，用 “%”

    cout << quotient << " " << remainder;
    
    return 0;
}
