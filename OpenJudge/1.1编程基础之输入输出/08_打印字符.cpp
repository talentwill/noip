// 08:打印字符
// http://noi.openjudge.cn/ch0102/08/

#include <iostream>

int main()
{
    int a;
    std::cin >> a; // 输入一个整形数字
    char b = a;    // 把这个数字赋值给字符类型
    std::cout << b << std::endl; // 输出这个字符
    return 0;
}