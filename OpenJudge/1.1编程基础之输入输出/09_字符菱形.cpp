// 09:字符菱形
// http://noi.openjudge.cn/ch0101/09/

#include <iostream>

using namespace std;

int main()
{
    char a;
    cin >> a;

    cout << "  " << a << endl;
    cout << " " << a << a << a << endl;
    cout << "" << a << a << a << a << a << endl;
    cout << " " << a << a << a << endl;
    cout << "  " << a << endl;

    return 0;
}