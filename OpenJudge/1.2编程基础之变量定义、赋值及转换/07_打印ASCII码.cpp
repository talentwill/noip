// 07:打印ASCII码
// http://noi.openjudge.cn/ch0102/07/

#include <iostream>
using namespace std;

int main()
{
    char a;
    cin >> a;
    cout << (int)a; // 强制类型转换
    return 0;
}