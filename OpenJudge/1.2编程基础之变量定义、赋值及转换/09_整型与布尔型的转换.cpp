// 09:整型与布尔型的转换
// http://noi.openjudge.cn/ch0102/09/

#include <iostream>
using namespace std;

int main() {
    
    int a;
    cin >> a;   // 1. 输入一个整型变量
    bool b = a; // 2. 把整型变量复制给布尔变量
    int c = b;  // 3. 再把布尔变量赋值给整型
    cout << c;

    return 0;
}
