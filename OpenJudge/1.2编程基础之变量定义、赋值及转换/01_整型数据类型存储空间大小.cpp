// 01:整型数据类型存储空间大小
// http://noi.openjudge.cn/ch0102/01/

#include <iostream>
using namespace std;

int main()
{
    int a;
    short b;
    cout << sizeof(a) << " " << sizeof(b);
    return 0;
}