// 02:浮点型数据类型存储空间大小 
// http://noi.openjudge.cn/ch0102/02/

#include <iostream>
using namespace std;

int main()
{
    float a;
    double b;
    cout << sizeof(a) << " " << sizeof(b);
    return 0;
}